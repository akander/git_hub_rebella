﻿using UnityEngine;
using System.Collections;

public class Guard_Spin : MonoBehaviour {

	Transform Player;
	
	Transform Prince;
	
	Transform Front;
	


	


	public Animator anim;

	// how fast we want the enemy to chase
	public float ChaseSpeed = 5f;
	
	// the range at which it detects Player
	public float Range = 5f;
	
	// what our current speed is (get only)
	float CurrentSpeed;


	// Use this for initialization
	void Start()
	{
		Player = FindObjectOfType<player_controller>().gameObject.transform;
		Prince = this.transform;
		anim = GetComponent<Animator>();
	}

	//make a angle between the player and the enemy and check if that is less than the set angle
	
	// Update is called once per frame
	void Update()
	{
		transform.Rotate (Vector3.forward * ChaseSpeed / 3);	
		Vector3 targetDir = Player.position - Prince.position;
		Vector3 inFront = transform.position - transform.up;
		float angle = Vector3.Angle(targetDir, inFront - transform.position);
		///  float angle = Vector3.Angle(targetDir, front);
		float changeAngle = 25f;
		
		
		
		if (Vector2.Distance(transform.position, Player.position) <= Range)// check the distance between this game object and Player and continue if it's less than Range
		{
			Debug.DrawRay(transform.position, targetDir);
			Debug.DrawRay(transform.position, inFront - transform.position);
			//    Debug.Log("Infront"+inFront);
			Debug.Log("inrange");
			if (changeAngle > angle)
			{
				//Debug.Log(angle);
				Debug.Log("sfhtrt");
				transform.rotation = Quaternion.LookRotation(Vector3.forward, -targetDir);
				CurrentSpeed = ChaseSpeed * Time.deltaTime; // set the CurrentSpeed to ChaseSpeed and multiply by Time.deltaTime (this prevents it from moving based on FPS)
				transform.position = Vector3.MoveTowards(transform.position, Player.position, CurrentSpeed);  // set this game objects position to the Player's position at the speed of CurrentSpeed
			}
			
		}
		
	}
	
}