﻿using UnityEngine;
using System.Collections;

public class StationaryController : MonoBehaviour {

	Transform Player;

	Transform Prince;

	//Rotate stationary guard
	public bool isRotating;
	public int rotateSpeed = 2;
	int i;

    public Transform Front;

    public Animator anim;



	public string faceDirecion;

	// how fast we want the enemy to chase
	public float ChaseSpeed = 5f;
	//walkspeed
	public float currentSpeed =1;
	// The angle he has
	public float changeAngle = 17f;

    // the range at which it detects Player
    public float Range = 5f;

    
	private Vector3 startPos;
	public bool chasing;
	bool tp = false; 

    // Use this for initialization
    void Start()
    {
		startPos = transform.position;
		chasing = false;
        Player = FindObjectOfType<player_controller>().gameObject.transform;
        anim = GetComponent<Animator>();
		Prince = this.transform;

		if (faceDirecion == "right") {
			Front.localPosition = Vector2.right/2;
		}
		if (faceDirecion == "left") {
			Front.localPosition = Vector2.left/2;
		}
		if (faceDirecion == "down") {
			Front.localPosition = Vector2.down/2;
		}
		if (faceDirecion == "up") {
			Front.localPosition = Vector2.up/2;
		}
		anim.SetBool (faceDirecion, true);
    }
    //make a angle between the player and the enemy and check if that is less than the set angle

    // Update is called once per frame
    void Update()
    {
		Vector3 targetDir = Player.position -  Prince.position;
		Vector3 inFront = Front.position;
		float angle = Vector3.Angle(targetDir, inFront-transform.position);
        ///  float angle = Vector3.Angle(targetDir, front);
	
        float changeAngle = 60f;
		//Debug.DrawRay(transform.position, inFront - transform.position);


		Ray2D lineOffSight = new Ray2D (Front.position, targetDir);
		RaycastHit2D hit = Physics2D.Raycast(Front.position,lineOffSight.direction);
		//Debug.DrawRay (Front.position, targetDir, Color.yellow);

        if (Vector2.Distance(transform.position, Player.position) <= Range)// check the distance between this game object and Player and continue if it's less than Range
        {
           // Debug.DrawRay(transform.position, targetDir);
            //Debug.DrawRay(transform.position, inFront - transform.position);
            //Debug.Log("Infront"+inFront);

          //  Debug.Log("inrange");
			//new Vector2(lineOffSight.origin.x,lineOffSight.origin.y)
			if (changeAngle > angle)
           {
		
				Debug.DrawRay(targetDir, inFront-transform.position);
				//Debug.Log ("" + hit.collider.gameObject.name);
				//if(Physics2D.Raycast(transform.position, lineOffSight.direction)){	
					if(hit.collider.CompareTag("Player")){
						chasing = true;
//						Debug.Log ("" + hit.collider.gameObject.name);
						transform.position = Vector3.MoveTowards(transform.position, Player.position, ChaseSpeed * Time.deltaTime);  // set this game objects position to the Player's position at the speed of CurrentSpeed
					}
				//}
			}
		} 
		else {
			if(!chasing){
			transform.position = Vector3.MoveTowards(transform.position, startPos, ChaseSpeed * Time.deltaTime);
			}
		}
		if (Vector2.Distance(transform.position, Player.position) <= 0.7f)// check the distance between this game object and Player and continue if it's less than Range
		{
			transform.position = Vector3.MoveTowards(transform.position, Player.position, ChaseSpeed * Time.deltaTime);  // set this game objects position to the Player's position at the speed of CurrentSpeed
		}
		else {
			//
			if(!chasing){
			transform.position = Vector3.MoveTowards(transform.position, startPos, ChaseSpeed * Time.deltaTime);
			}
		}
		if (Vector2.Distance(transform.position, Player.position) >= Range)// check the distance between this game object and Player and continue if it's less than Range
		{
//			Debug.Log("far");
			changeAngle = 45f;
			Range = 3f;
			chasing = false;
		}

		if(isRotating)
		{
			StartCoroutine(RotateGuard());
		}

        }
	IEnumerator RotateGuard(){
		if(i == 0){
			i = 1;
			anim.SetBool (faceDirecion, false);
			faceDirecion = "left";
			Front.localPosition = Vector2.left/2;
			anim.SetBool (faceDirecion, true);
		yield return new WaitForSeconds (rotateSpeed);
			anim.SetBool (faceDirecion, false);
			faceDirecion = "up";
			Front.localPosition = Vector2.up/2;
			anim.SetBool (faceDirecion, true);
		yield return new WaitForSeconds (rotateSpeed);
			anim.SetBool (faceDirecion, false);
			faceDirecion = "right";
			Front.localPosition = Vector2.right/2;
			anim.SetBool (faceDirecion, true);
		yield return new WaitForSeconds (rotateSpeed);
			anim.SetBool (faceDirecion, false);
			faceDirecion = "down";
			Front.localPosition = Vector2.down/2;
			anim.SetBool (faceDirecion, true);
		yield return new WaitForSeconds (rotateSpeed);
			i = 0;
		}
	}
	IEnumerator Reset(){
		
		if(chasing)
		{
			//Debug.Log ("chasing");
			tp = true;
		} 
		else if(tp)
		{
			tp = false;
			//Debug.Log("tp");
			yield return new WaitForSeconds(1f);
			Teleport();
			chasing = false;
		}
	}
	void Teleport(){
		this.gameObject.transform.position = startPos;
	}
}