﻿using UnityEngine;
using System.Collections;

public class Square_Enemy_Right : MonoBehaviour {
	
	Transform Player;
	
	Transform Prince;
	
	public Transform Front;
	
	
	public Transform Waypoint1;
	public Transform Waypoint2;
	public Transform Waypoint3;
	public Transform Waypoint4;
	
	public Transform WP;
	
	
	
	public Animator anim;
	
	// how fast we want the enemy to chase
	public float ChaseSpeed = 5f;
	//walkspeed
	public float currentSpeed =1;
	// The angle he has
	public float changeAngle = 17f;
	
	// the range at which it detects Player
	public float Range = 5f;
	
	// what our current speed is (get only)
	float CurrentSpeed;
	private Vector3 startPos;
	public bool chasing;
	public bool chasingWP;
	bool tp = false; 
	
	
	// Use this for initialization
	void Start () {
		startPos = transform.position;
		chasing = false;
		Player = FindObjectOfType<player_controller>().gameObject.transform;
		Prince = this.transform;
		anim = GetComponent<Animator>();
		Front.localPosition = Vector2.right/4;
		WP.position = Waypoint1.position;
		chasingWP = false;
	}
	//make a angle between the player and the enemy and check if that is less than the set angle
	
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if (chasing == false)
		{
			if (col.tag == "WP2")
			{
			WP.position = Waypoint1.position;
			Front.localPosition = Vector2.left/4;
			
			anim.SetBool("up", false);
			anim.SetBool("down", false);
			anim.SetBool("right", false);
			anim.SetBool("left", true);
			}
		}
		if (chasing == false)
		{
			if (col.tag == "WP1")
			{
			WP.position = Waypoint3.position;
			Front.localPosition = Vector2.down/4;
			
			anim.SetBool("up", false);
			anim.SetBool("down", true);
			anim.SetBool("right", false);
			anim.SetBool("left", false);
			}
		}
		if (chasing == false)
		{
			if (col.tag == "WP3") 
			{
			WP.position = Waypoint4.position;
			Front.localPosition = Vector2.right/4;
			
			anim.SetBool("up", false);
			anim.SetBool("down", false);
			anim.SetBool("right", true);
			anim.SetBool("left", false);
			}
		}
		if (chasing == false)
		{
			if (col.tag == "WP4") 
			{
			WP.position = Waypoint2.position;
			Front.localPosition = Vector2.up/4;
			
			anim.SetBool("up", true);
			anim.SetBool("down", false);
			anim.SetBool("right", false);
			anim.SetBool("left", false);
			}
		}
	}
	
	
	
	
	// Update is called once per frame
	void Update () {
		Vector2 targetDir = Player.position -  Front.position;
		Vector2 inFront = Front.position;
		Vector2 forw = Front.position - Prince.position;
		//float angle = Vector2.Angle(inFront, targetDir);
		// float angle = Vector3.Angle(targetDir, front);
		transform.position = Vector3.MoveTowards(transform.position, WP.position, currentSpeed * Time.deltaTime);
		//Debug.DrawRay(transform.position, inFront-transform.position);
		
		Ray2D lineOffSight = new Ray2D (Front.position, targetDir);
		RaycastHit2D hit = Physics2D.Raycast(Front.position,lineOffSight.direction);
		Debug.DrawRay (Front.position, targetDir, Color.green);
		//Debug.Log(hit.collider.gameObject.name);
		
		
		if (Vector2.Distance(transform.position, Player.position) <= Range)// check the distance between this game object and Player and continue if it's less than Range
		{
//			Debug.DrawRay(transform.position, targetDir);
//			Debug.DrawRay(forw, targetDir, Color.red);
//			Debug.DrawRay(Front.position, Prince.position, Color.blue);
			//Debug.Log("Infront"+inFront);
			//Debug.Log("inrange");
			//Debug.Log(" hi" + Vector2.Angle(forw, targetDir));
			if (Vector2.Angle(forw, targetDir) < changeAngle)
			{
				//	Debug.Log ("loc");
//				Debug.Log (hit.collider.gameObject.name);
				if(hit.collider.CompareTag("Player")){
					//Debug.Log("");
					transform.position = Vector3.MoveTowards(transform.position, Player.position, ChaseSpeed * Time.deltaTime);  // set this game objects position to the Player's position at the speed of CurrentSpeed
					chasing = true;
					chasingWP = true;
					changeAngle = 75f;
					Range = 5f;
				}
			} else {
				changeAngle = 17f;
				Range = 4f;
				chasingWP = false;
			}
			if (Vector2.Distance(transform.position, Player.position) <= 0.5f)// check the distance between this game object and Player and continue if it's less than Range
			{
				transform.position = Vector2.MoveTowards(transform.position, Player.position, ChaseSpeed * Time.deltaTime);  // set this game objects position to the Player's position at the speed of CurrentSpeed
			} else {
				chasingWP = false;
			}

		}
		if (Vector2.Distance(transform.position, Player.position) >= Range)// check the distance between this game object and Player and continue if it's less than Range
		{
//			Debug.Log("far");
			changeAngle = 45f;
			Range = 4f;
			chasing = false;
		}
		//if (Vector2.Distance(transform.position, Prince.position) <= Range - 1f)// check the distance between this game object and Player and continue if it's less than Range
		//{
		//Debug.DrawRay(transform.position, targetDir);
		//Debug.DrawRay(transform.position, inFront - transform.position);
		//    Debug.Log("Infront"+inFront);
		//Debug.Log("inrange");
		//if (changeAngle > angle)
		//{
		//Debug.Log(angle);
		//	Debug.Log("sfhtrt");
		//	Debug.Log("Yo!");
		//}
		
		//}
		
		
		
	}
	IEnumerator Reset(){
		
		if(chasing)
		{
			//				Debug.Log ("chasing");
			tp = true;
		} 
		else if(tp)
		{
			tp = false;
			//				Debug.Log("tp");
			yield return new WaitForSeconds(1f);
			Teleport();
			chasing = false;
		}
	}
	void Teleport(){
		this.gameObject.transform.position = startPos;
	}
	
}