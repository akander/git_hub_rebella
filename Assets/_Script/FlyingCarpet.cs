﻿using UnityEngine;
using System.Collections;

public class FlyingCarpet : MonoBehaviour {
	public Transform Prince;
	public GameObject PrinceSprite;
	public GameObject hole;

	public Transform WPGO;
	public Transform WPUP;
	public Transform Waypoint1;
	public Transform Waypoint2;
	public Transform Waypoint3;
	public Transform Waypoint4;
	public Transform WP;
	Transform flCarpet;
	public float currentSpeed = 1;
	int i = 0;
	public Animator anim;
	public bool side = false;
	public bool up = false;

	// Use this for initialization
	void Start () {
		WP.position = WPGO.position;
		anim = GetComponent<Animator>();
		PrinceSprite.SetActive(false);
		flCarpet = this.transform;
	}
	
	void OnTriggerEnter2D(Collider2D col)
	{
	
		if (col.tag == "WPGO")
		{
			WP.position = WPUP.position;
		}

		if (col.tag == "WPUP")
		{
			WP.position = Waypoint1.position;
			flCarpet.Rotate(0, 0, 90);
			up = true;
		}

			if (col.tag == "WP2")
			{
				if(up)
				{
					WP.position = Waypoint4.position;
					flCarpet.Rotate(0, 0, 90);
					up = false;
					side = true;
				}
			}
		

			if (col.tag == "WP1")
			{
				if(side)
				{
					WP.position = Waypoint2.position;
					flCarpet.Rotate(0, 0, -90);
					up = true;
					side = false;
				}
			}


			if (col.tag == "WP3") 
			{ 
			if(up)
			{
			if(i == 1)
			{
				WP.position = Waypoint1.position;
				flCarpet.Rotate(0, 0, 90);
				up = false;
				side = true;
				} else {
					i = 1;
					WP.position = Waypoint1.position;
					side = true;
					}	
				}
			}

			if (col.tag == "WP4") 
			{
				if(side)
				{
					WP.position = Waypoint3.position;
					flCarpet.Rotate(0, 0, -90);
					up = true;
					side = false;
				}
			}
	}

		void OnCollisionEnter2D(Collision2D col){
			
			if (col.collider.CompareTag ("Prince"))
			{
				PrinceSprite.SetActive(true);
				Destroy(col.gameObject);
				
			}
		if (col.collider.tag == "Hole")
			{
				PrinceSprite.SetActive(false);
				hole.GetComponent<HoleScript>().Ignore();
			}
		}


	

	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.MoveTowards(transform.position, WP.position, currentSpeed * Time.deltaTime);
	}
}
