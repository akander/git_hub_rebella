﻿using UnityEngine;
using System.Collections;

public class RosePlacement : MonoBehaviour {

	public GameObject potion_GO;
	
	private Vector3 potionPos;
	
	public static int potionCounter_Int = 0;
	
	private GameObject tempPotion_GO;
	
	public GameObject potionUI;
	
	
	private bool placeToTheRight_Bool;
	
	private bool placeAbove_Bool;
	
	private bool placeToTheLeft_Bool;
	
	private bool placeUnder_Bool;
	
	
	void Update()
	{
		SpawnPotion ();
	}
	
	void OnCollisionEnter2D(Collision2D col){
		
		if (col.collider.CompareTag ("Rose")) {
			potionCounter_Int++;
			potionUI.SetActive(true);
			col.gameObject.SetActive(false);
			
			//watch the order of what is about to be done
		}
		
	}
	
	private void SpawnPotion ()
	{
		if (Input.GetKeyDown (KeyCode.E) && potionCounter_Int >= 1) 
		{
			potionPos = transform.position;
			potionPos.x = potionPos.x + 1f;
			
			tempPotion_GO = Instantiate (potion_GO, potionPos, Quaternion.identity) as GameObject;
			//tempPotion_GO.transform.tag = "UnPickable";
			
			
			--potionCounter_Int;
			potionUI.SetActive(false);
			Debug.Log ("potion counter : " + potionCounter_Int);
			
		}
		
	}

}
