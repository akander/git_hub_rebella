﻿using UnityEngine;
using System.Collections;

public class MagicLamp : MonoBehaviour {

	public GameObject flyingCarpet;
	// Use this for initialization
	void Start () {
		flyingCarpet.SetActive(false);
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.collider.tag == "Player")
		{
			flyingCarpet.SetActive(true);
			Destroy(this.gameObject);
		}
	}
	// Update is called once per frame
	void Update () {
	
	}
}
