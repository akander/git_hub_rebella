﻿using UnityEngine;
using System.Collections;

public class RoseTrap : MonoBehaviour {
	//public Sprite newSprite;
	//private SpriteRenderer spriteRend;
	public GameObject frog;
	//public GameObject collision_GO;
	public Animator anim;
	public GameObject prince;
	
	// Use this for initialization
	void Start () {
		//spriteRend = GetComponent<SpriteRenderer> ();
		prince = GameObject.FindGameObjectWithTag("Prince");
		anim = prince.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnCollisionEnter2D(Collision2D col)
	{
		Debug.Log ("PotionTrap Coll");
		Debug.Log (col.collider.tag);
		if (col.collider.CompareTag ("Prince"))
		{
			Debug.Log (col.collider.tag);
			//collision_GO = col.collider.gameObject;
			StartCoroutine(rose());
			//Instantiate(frog,col.gameObject.transform.position,Quaternion.identity);
			//Destroy(col.gameObject);
			//col.collider.gameObject.GetComponent<SpriteRenderer>().sprite = newSprite;
			//this.gameObject.transform.GetComponent<SpriteRenderer>().sprite = newSprite;
			//Destroy this
			//Destroy (this.gameObject);
		}
	}
	IEnumerator rose()
	{

		anim.SetBool("Left", false);
		anim.SetBool("Right", false);
		anim.SetBool("Up", false);
		anim.SetBool("Down", false);
		anim.SetBool("Rose", true);
		this.gameObject.transform.position = new Vector2(200f,200f);
		yield return new WaitForSeconds(1.4f);
		Instantiate(frog,prince.gameObject.transform.position,Quaternion.identity);
		Destroy(prince.gameObject);
		Destroy(this.gameObject);


	}
}
