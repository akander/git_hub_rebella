﻿using UnityEngine;
using System.Collections;

public class PotionTrap : MonoBehaviour {

	public Sprite newSprite;
	private SpriteRenderer spriteRend;
	public GameObject frog;
	public GameObject ParticleC;
	//public GameObject collision_GO;

	// Use this for initialization
	void Start () {
		spriteRend = GetComponent<SpriteRenderer> ();
		ParticleC.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		Debug.Log ("PotionTrap Coll");
		Debug.Log (col.collider.tag);
		if (col.collider.CompareTag ("Enemy") || col.collider.CompareTag("EnemyTL") || col.collider.CompareTag("EnemyHor") || col.collider.CompareTag("Prince") )
		{
			ParticleC.SetActive(true);
			Debug.Log (col.collider.tag);
			//collision_GO = col.collider.gameObject;
			//Aply effect
			Instantiate(frog,col.gameObject.transform.position,Quaternion.identity);
			Destroy(col.gameObject);
			//col.collider.gameObject.GetComponent<SpriteRenderer>().sprite = newSprite;
			//this.gameObject.transform.GetComponent<SpriteRenderer>().sprite = newSprite;
			//Destroy this
			Destroy(ParticleC.gameObject, 0.5f);
			Destroy (this.gameObject, 0.5f);

		}
	}
}
