﻿using UnityEngine;
using System.Collections;

public class HoleScript : MonoBehaviour {
	public Transform Carpet;
	
	public void Ignore() {
		Physics2D.IgnoreCollision(Carpet.GetComponent<Collider2D>(), this.gameObject.GetComponent<Collider2D>());
	}
}
