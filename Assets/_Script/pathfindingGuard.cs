﻿using UnityEngine;
using System.Collections;

public class pathfindingGuard : MonoBehaviour {

	public Transform target;
	NavMeshAgent agent;


	// Use this for initialization
	void Start () {
		agent = this.gameObject.GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
	
		agent.SetDestination(target.transform.position);

	}
}
