﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Potion : MonoBehaviour {

	public GameObject potion_GO;
	
	private Vector3 potionPos;
	
	public static int potionCounter_Int = 0;
	
	private GameObject tempPotion_GO;

	public GameObject potionUI;

	public Animator anim;

	public float throwforce;

	public Rigidbody2D RB;


	void Start()
	{
		anim = GetComponent<Animator>();
		
	}


	void Update()
	{
		SpawnPotion ();
		RB.AddForce(new Vector2(0,50));
	}

	void OnCollisionEnter2D(Collision2D col){
		
		if (col.collider.CompareTag ("Potion")) {
			potionCounter_Int++;
			potionUI.SetActive(true);
			col.gameObject.SetActive(false);

			//watch the order of what is about to be done
		}
		
	}

	private void SpawnPotion ()
	{
		if (Input.GetKeyDown (KeyCode.E) && potionCounter_Int >= 1) //when pressed E AND if the potion counter is 1 or more.
		{

			if (anim.GetBool("WalkUpIdle")) {
				potionPos = transform.position;
				potionPos.y = potionPos.y + 1f;
				tempPotion_GO = Instantiate (potion_GO, potionPos, Quaternion.identity) as GameObject;
				RB.velocity= new Vector2(transform.localScale.y,1)*throwforce;
			//	RB.AddForce(new Vector2(0,50));
			}

			if (anim.GetBool("WalkDownIdle")) {
				potionPos = transform.position;
				potionPos.y = potionPos.y + -1f;
				tempPotion_GO = Instantiate (potion_GO, potionPos, Quaternion.identity) as GameObject;
				RB.velocity= new Vector2(transform.localScale.y,-1)*throwforce;
			}

			if (anim.GetBool("WalkRightIdle")) {
				potionPos = transform.position;
				potionPos.x = potionPos.x + 1f;
				tempPotion_GO = Instantiate (potion_GO, potionPos, Quaternion.identity) as GameObject;
				RB.velocity= new Vector2(transform.localScale.x,1)*throwforce;
			}

			if (anim.GetBool("WalkLeftIdle")) {
				potionPos = transform.position;
				potionPos.x = potionPos.x + -1f;
				tempPotion_GO = Instantiate (potion_GO, potionPos, Quaternion.identity) as GameObject;
				RB.velocity= new Vector2(transform.localScale.x,-1)*throwforce;
			}

			//What animation is playering, that way the potion drops.
			
			//tempPotion_GO = Instantiate (potion_GO, potionPos, Quaternion.identity) as GameObject;
			//tempPotion_GO.transform.tag = "UnPickable";
			
			
			--potionCounter_Int;
			potionUI.SetActive(false);
			Debug.Log ("potion counter : " + potionCounter_Int);

			//When potion is dropped, the potion counter -1.
			
		}
		
	}
	
}
