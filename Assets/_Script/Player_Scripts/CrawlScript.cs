﻿using UnityEngine;
using System.Collections;

public class CrawlScript : MonoBehaviour {

	public Animator anim;
	private float movex = 0f;
	private float movey = 0f;

	void Start(){
		anim = GetComponent<Animator>();

	}
	void FixedUpdate()
	{
		movex = Input.GetAxis ("Horizontal");
		movey = Input.GetAxis ("Vertical");
//		
//		if (movex > 0)
//		{
//			anim.SetBool ("crawl", true);
//		} else {
//			anim.SetBool ("crawl", false);
//		}
//
//		if (movex < 0) 
//		{
//			anim.SetBool ("crawl", true);
//		} else {
//			anim.SetBool ("crawl", false);
//			}
//		if (movey > 0) 
//			{
//			anim.SetBool ("crawl", true);
//		} else {
//			anim.SetBool ("crawl", false);
//		}
//		if (movey < 0) 
//				{
//			anim.SetBool ("crawl", true);
//		} else {
//			anim.SetBool ("crawl", false);
//			}
		//
		//
		if (movey > 0) {
			anim.SetBool ("crawlup", true);
		} else {
			anim.SetBool ("crawlup", false);
		}
		
		if (movey < 0) {
			anim.SetBool ("crawldown", true);
		} else {
			anim.SetBool ("crawldown", false);
		}
		
		if (movex > 0) {
			anim.SetBool ("crawlright", true);
		} else {
			anim.SetBool ("crawlright", false);
		}
		
		if (movex < 0) {
			anim.SetBool ("crawlleft", true);
		} else {
			anim.SetBool ("crawlleft", false);
		}
	}
}