﻿using UnityEngine;
using System.Collections;

public class ArmorScript : MonoBehaviour {
	public Animator anim;
	private float movex = 0f;
	private float movey = 0f;
	void Start(){
		anim = GetComponent<Animator>();
		
	}
	void FixedUpdate(){

		movex = Input.GetAxis ("Horizontal");
		movey = Input.GetAxis ("Vertical");

//					if (movex > 0)
//					{
//						anim.SetBool ("crawl", true);
//					} else {
//						anim.SetBool ("crawl", false);
//					}
//			
//					if (movex < 0) 
//					{
//						anim.SetBool ("crawl", true);
//					} else {
//						anim.SetBool ("crawl", false);
//						}
//					if (movey > 0) 
//						{
//						anim.SetBool ("crawl", true);
//					} else {
//						anim.SetBool ("crawl", false);
//					}
//					if (movey < 0) 
//							{
//						anim.SetBool ("crawl", true);
//					} else {
//						anim.SetBool ("crawl", false);
//						}


		if (movey > 0) {
			anim.SetBool ("up", true);
		} else {
			anim.SetBool ("up", false);
		}
		
		if (movey < 0) {
			anim.SetBool ("down", true);
		} else {
			anim.SetBool ("down", false);
		}
		
		if (movex > 0) {
			anim.SetBool ("right", true);
		} else {
			anim.SetBool ("right", false);
		}
		
		if (movex < 0) {
			anim.SetBool ("left", true);
		} else {
			anim.SetBool ("left", false);
		}
	}
}