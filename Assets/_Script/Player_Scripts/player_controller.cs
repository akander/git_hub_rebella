﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class player_controller : MonoBehaviour
{
	public Animator anim;
	public GameObject player_GO;//it is the player Game object	
	public GameObject LampShade;// it is the object used to hide the player
	public GameObject Lamp;//it is the object to pick up the hiding object from
	private bool LampExit;// it is the bool for if you are inside the area of the hide object or not
	private bool ArmorExit;// it is the bool for if you are inside the area of the hide object or not
	public GameObject ArmorStand;//it is the object to pick up the hiding object from
	public GameObject ArmorHead;// it is the object used to hide the player

	public GameObject caughtCanvas;
	public GameObject caughtCanvasP;

    public float speed = 0f;//players movement speed
	private float movex = 0f;//variables to move the player
	private float movey = 0f;//variables to move the player

    public int keys = 0;//the amoung of yellow keys the player has
	public int blueKeys = 0;//the amount of blue keys the player has
	public int redKeys = 0;//the amount of red keys the player has
	public int soap = 0;

    public static int lives = 3;//the amount of lives the player has

	public GameObject button;//the button for showing interactable object

	public int i = 0;
	int o = 0;

	bool placed;
	//public Vector3 startPos;

	public Transform Health;//the health sprite
	public Transform InvKey;//yellow key sprite
	public Transform BlueKey;//blue key sprite
	public Transform RedKey;//red key sprite
	public Transform saop;
	public Transform FightCloud;//the deathanimation object
	public Transform FightPrince;//the fight prince death animation

	public Rigidbody2D rb;//the rigidbody in which we add force
	bool hidding;//bool for if the player is hiding or not
	bool moving;//the bool to check if the player is moving
	public Transform hatcollider1;//the collider that blocks raycast
	public Transform hatcollider2;//the collider that blocks raycast


	//used to check if the player is moving
	public float xaxis;
	public float yaxis;

	public float xaxisOld;
	public float yaxisOld;

	private GameObject key_GO;

	//SpriteRenderer spriteRend;

	//public GameObject gameOver;
	//public GameObject Clearlvl;

	//stuff for potion
	public int potionCounter_Int;
	public bool ChestOpen = false; //the bool to check if the blue chest has been unlocked by a key =)
	private GameObject tempPotion_GO;
	public float throwforce;
	public GameObject potion_GO;
	private Vector3 potionPos;
	public GameObject potionUI;
	bool dead = false; 
	bool time_bool = false;
	bool isPaused = false;
	public GameObject soap_GO;

    void Start()
    {
		caughtCanvas.SetActive (false);
		caughtCanvasP.SetActive (false);
		ArmorExit = false;//setting the bool to false at start
		LampExit = false;//setting the bool to false at start
		rb = GetComponent<Rigidbody2D> ();//getting the players rigidbody 
		//spriteRend = player_GO.GetComponent<SpriteRenderer>();
    }
		 

	void Update(){

		if (time_bool){

			Time.timeScale = 0.0f;
		} else {
			Time.timeScale = 1.0f;
		}

		if (dead)
		{
			if(Input.GetKeyDown(KeyCode.R))
			{
				lives--;
				Health.GetComponent<HealthScript> ().LoseHealth ();
				Application.LoadLevel(Application.loadedLevel);
				//Time.timeScale = 1f;
				caughtCanvas.SetActive (false);
				caughtCanvasP.SetActive (false);

			}

		}


		StartCoroutine(SpawnPotion ());


		xaxis = transform.position.x;//always changing the value of xaxis
		yaxis = transform.position.y;//always changing the value of xaxis

		move ();//always getting the move function

		if (LampExit == true) //if inside the collider of the object do this
		{

			StartCoroutine(hidelamp());//start the function hidelamp

		}
		if (ArmorExit == true)//if inside the collider of the object do this
		{
			StartCoroutine(hideArmor());//start the function hidearmor
		}
	}
		// Update is called once per frame
	void FixedUpdate () {
		if (i == 0) {
			movex = Input.GetAxis ("Horizontal");//getting the horizontal axis for the movement
			movey = Input.GetAxis ("Vertical");//getting the vertical axis for the movement
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (movex * speed, movey * speed);//adding force to the rigidbody in the direction inputed
			StartCoroutine(movePlayer());//start the function movePlayer

		}

	}
    void GameOver ()
    {
		//gameOver.SetActive(true);
		Application.LoadLevel ("GameOver");//if you die load the game over screen
		Time.timeScale = 0.0F;//set the time scale to 0 to be safe
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.CompareTag("Key"))//if you hit a key do this
        {
			GetComponent<Animationcontroller>().Weed();
            //Debug.Log("Hit");
            keys++;//you get a key
			StartCoroutine(FuckLife());
            Destroy(col.gameObject,0.3f);//destroy the key game object
			InvKey.GetComponent<KeyController>().GetKey();//accessing the sprite controller of the key
        }

		if (col.collider.CompareTag("BlueKey"))//if you hit a key do this
		{
			GetComponent<Animationcontroller>().Weed();
			//Debug.Log("Hit");
			StartCoroutine(FuckLife());
			blueKeys++;//you get a key
			Destroy(col.gameObject, 0.3f);//destroy the key game object
			BlueKey.GetComponent<BlueKeyController>().GetKey();//accessing the sprite controller of the key
		}

		if (col.collider.CompareTag("RedKey"))//if you hit a key do this
		{
			GetComponent<Animationcontroller>().Weed();
			//Debug.Log("Hit");
			StartCoroutine(FuckLife());
			redKeys++;//you get a key
			Destroy(col.gameObject, 0.3f);//destroy the key game object
			RedKey.GetComponent<RedKeyController>().GetKey();//accessing the sprite controller of the key
		}

//		if (col.collider.CompareTag("SceneChanger"))
//		{
//			Clear ();
//		}


		if (col.collider.CompareTag("Door"))//if you hit a door then this happens
		{
			if(keys >= 1)//if you have 1 or more keys this happens
			{
				//Debug.Log("Lock");
				Destroy(col.gameObject);//destroy the door
				keys--;//use a key
				InvKey.GetComponent<KeyController>().UseKey();//change the sprite of the key to one less
			}
		}
		if (col.collider.CompareTag("BlueChest"))
		{//if you hit a blue chest then this happens
			if (blueKeys > 0){//if you have 1 or more keys this happens
				ChestOpen = true;
//				keys++;//get a yellow key
//				blueKeys--;//lose a blue key
//				InvKey.GetComponent<KeyController>().GetKey();//change the sprite of the key to one more
				BlueKey.GetComponent<BlueKeyController>().UseKey();//change the sprite of the key to one less
			} 
			if (ChestOpen && potionCounter_Int == 0)
			{
				potionCounter_Int = 1;
				potionUI.SetActive(true);
				//give player potion =)
			}
		}
		if (col.collider.CompareTag("RedChest")){//if you hit a red chest then this happens
			if (redKeys >= 1){//if you have 1 or more keys this happens
				
				blueKeys++;//you get a blue key
				redKeys--;//you lose a red key
				BlueKey.GetComponent<BlueKeyController>().GetKey();//change the sprite of the key to one more
				RedKey.GetComponent<RedKeyController>().UseKey();//change the sprite of the key to one less
			}
		}

		if (col.collider.CompareTag("BlueDoor")){//if you hit a blue chest then this happens
			if (blueKeys >= 1){//if you have 1 or more keys this happens
				Destroy(col.gameObject);//destroy the door
				blueKeys--;//lose a blue key
				BlueKey.GetComponent<BlueKeyController>().UseKey();//change the sprite of the key to one less
			}
		}
		if (col.collider.CompareTag("RedDoor")){//if you hit a blue chest then this happens
			if (redKeys >= 1){//if you have 1 or more keys this happens
				Destroy(col.gameObject);//destroy the door
				redKeys--;//lose a blue key
				RedKey.GetComponent<RedKeyController>().UseKey();//change the sprite of the key to one less
			}
		}

		if (col.collider.CompareTag("Enemy") || col.collider.CompareTag("EnemyTL") || col.collider.CompareTag("EnemyHor"))//if you hit an enemy this happens
        {
			//Debug.Log("hit");
			StartCoroutine(Fight());//you start the the function fight
			Destroy(col.gameObject);//you destroy the enemy
			rb.drag = 9999999;//set the drag of the rigidbody to 3000 so you can't move
		
		}
		if (col.collider.CompareTag("Prince"))//if you hit an prince this happens
		{
			//Debug.Log("hit");

			StartCoroutine(FightP());//you start the the function fightP
			Destroy(col.gameObject);//you destroy the prince
			rb.drag = 9999999;//set the drag of the rigidbody to 3000 so you can't move
			
		}
		if (col.collider.CompareTag ("Potion")) {
			potionCounter_Int = 1;
			potionUI.SetActive(true);
			Destroy(col.gameObject);
			
			//watch the order of what is about to be done
		}
		if (col.collider.CompareTag("soap"))//if you hit a key do this
		{
			GetComponent<Animationcontroller>().Weed();
			//Debug.Log("Hit");
			StartCoroutine(FuckLife());
			soap++;//you get a key
			Destroy(col.gameObject, 0.3f);//destroy the key game object
			saop.GetComponent<SoapController>().Get();//accessing the sprite controller of the key
		}
    }

	IEnumerator Fight()
	{
		//Debug.Log ("Fight");
		dead = true;
		yield return new WaitForSeconds (0.2f);	//wait for 0.2 seconds
		FightCloud.GetComponent<fightAniScript> ().FightCloud ();//activate the fight cloud function
		yield return new WaitForSeconds (2f);	//wait for another 2 seconds
		time();
		if (lives <= 1) {//if you have less the 0 health this happens
			//Debug.Log ("Dead");
			GameOver ();//activates the gameover function
		} else {//if you have more than or equal to 0 health this happens instead
			caughtCanvas.SetActive (true);
//			Health.GetComponent<HealthScript> ().LoseHealth ();//change the sprite of helth to one less
			//lives--;//lose a life
			Time.timeScale = 0.0f;
		}
	}
	IEnumerator FightP()
	{
		Debug.Log ("FightP");
		dead = true;
		//yield return new WaitForSeconds (0.2f);	
		//spriteRend.SetActive(false);
	//	player_GO.transform.position += new Vector3(0f,0f,10f);
		FightPrince.GetComponent<fightAniScript> ().FightCloud ();//activate the fight prince function
		yield return new WaitForSeconds (3f);	//wait for 3 seconds
		if (lives <= 1) {//if you have less the 0 health this happens
			//Debug.Log ("Prince killed me Dead");
			GameOver ();//activates the gameover function
		} else {//if you have more than or equal to 0 health this happens instead
			caughtCanvasP.SetActive (true);
			//Health.GetComponent<HealthScript> ().LoseHealth ();//change the sprite of helth to one less
			//lives--;//lose a life
			//Application.LoadLevel (Application.loadedLevel);//restart the level
			Time.timeScale = 0.0f;
		}
	}



	void OnTriggerEnter2D(Collider2D col){
		if (col.CompareTag("lamp")) {//if you are inside the area of the lamp then do this
			LampExit = true;//set the bool to true
			button.SetActive(true);//set the incteraction button active
		}
		if (col.CompareTag("ArmorStand")) {//if you are inside the area of the armor then do this
			ArmorExit = true;//set the bool to true
			button.SetActive(true);//set the incteraction button active
		}
		if (col.CompareTag("poisonApple")){
			if(o == 0){
				o = 1;
				GameObject coffin = GameObject.FindGameObjectWithTag("coffin");
				coffin.GetComponent<CoffinController>().strenght = true;
				Debug.Log("My Apple");
				Destroy(col.gameObject);

			}
		}

	}
	void OnTriggerExit2D(Collider2D col){
		 //if you leave the lamp or armor area put all these to false
		LampExit = false;
		button.SetActive (false);
		ArmorExit = false;
			

	}


	public void Startgame()
	{
		
		lives = 3;//start with 3 life when you start
		Health.GetComponent<HealthScript>().FullHealth();//set your health visua to full
	}



	IEnumerator hidelamp()
	{
		if (hidding == false) 
		{
			if (Input.GetKey (KeyCode.Space)) {//taking the lampshade and it getting reset after 10 sec
				yield return new WaitForSeconds(0.5f);
				LampShade.SetActive (true);
				//Debug.Log ("swag123");
				Lamp.SetActive (false);
				hidding = true;
				button.SetActive(false);
				yield return new WaitForSeconds(10f);
				LampShade.SetActive (false);
				//Debug.Log ("swag123");
				Lamp.SetActive (true);
				hidding = false;
			}
		}	

	}

	IEnumerator hideArmor()
	{
		if (hidding == false)
		{
			if (Input.GetKey (KeyCode.Space))//taking the armorpiece and it getting reset after 10 sec
			{
				yield return new WaitForSeconds(0.5f);
				ArmorHead.SetActive(true);
				Debug.Log ("swag123");
				ArmorStand.SetActive(false);
				hidding = true;
				//button.SetActive(false);
				yield return new WaitForSeconds(5f);
				ArmorHead.SetActive(false);
				Debug.Log ("swag123");
				ArmorStand.SetActive(true);
				hidding = false;
			}
		}


	}


	void move(){

		if (moving == true) {//if you are moving set the scale of the collider on the hat to 0.8 if not leave it at 1
			hatcollider1.localScale = new Vector3(0.8f,0.8f,0.8f);
			hatcollider2.localScale = new Vector3(0.8f,0.8f,0.8f);	
		
		}
		else
		{
		
			hatcollider1.localScale = new Vector3(1f,1f,1f);
			hatcollider2.localScale = new Vector3(1f,1f,1f);

		}
	
	}

	IEnumerator movePlayer()//is the player moving
	{
		xaxisOld = xaxis;
		yaxisOld = yaxis;
		yield return new WaitForSeconds (0.05f);
		if (xaxisOld == xaxis && yaxisOld == yaxis) 
		{
			moving = false;
		} 
		else 
		{
			moving = true;
		}

	}

	public void ResetLevel(){
		lives--;
		Health.GetComponent<HealthScript> ().LoseHealth ();
		Application.LoadLevel(Application.loadedLevel);
		//Time.timeScale = 1f;
		caughtCanvas.SetActive (false);
		caughtCanvasP.SetActive (false);
	}
	private IEnumerator SpawnPotion ()
	{
		if (Input.GetKeyDown (KeyCode.E) && potionCounter_Int == 1) //when pressed E AND if the potion counter is 1 or more.
		{
			potionCounter_Int = 0;

			if (anim.GetBool("WalkUpIdle")) {
				potionPos = transform.position;
				potionPos.y = potionPos.y + 1f;
				tempPotion_GO = Instantiate (potion_GO, potionPos, Quaternion.identity) as GameObject;
				potionUI.SetActive(false);
				rb.velocity= new Vector2(transform.localScale.y,1)*throwforce;
				//	RB.AddForce(new Vector2(0,50));
			}
			
			if (anim.GetBool("WalkDownIdle")) {
				potionPos = transform.position;
				potionPos.y = potionPos.y + -1f;
				tempPotion_GO = Instantiate (potion_GO, potionPos, Quaternion.identity) as GameObject;
				rb.velocity= new Vector2(transform.localScale.y,-1)*throwforce;
				potionUI.SetActive(false);
			}
			
			if (anim.GetBool("WalkRightIdle")) {
				potionPos = transform.position;
				potionPos.x = potionPos.x + 1f;
				tempPotion_GO = Instantiate (potion_GO, potionPos, Quaternion.identity) as GameObject;	
				rb.velocity= new Vector2(transform.localScale.x,1)*throwforce;
				potionUI.SetActive(false);
			}
			
			if (anim.GetBool("WalkLeftIdle")) {
				potionPos = transform.position;
				potionPos.x = potionPos.x + -1f;
				tempPotion_GO = Instantiate (potion_GO, potionPos, Quaternion.identity) as GameObject;
				rb.velocity= new Vector2(transform.localScale.x,-1)*throwforce;
				potionUI.SetActive(false);
			}
			//++potionCounter_Int;
			yield return new WaitForSeconds (5f);	
			//--potionCounter_Int;
			Debug.Log ("potion counter : " + potionCounter_Int);
			
			//When potion is dropped, the potion counter -1.
			
		}
		if (Input.GetKeyDown (KeyCode.E) && soap == 1) //when pressed E AND if the potion counter is 1 or more.
		{
			soap = 0;
			
			if (anim.GetBool("WalkUpIdle")) {
				potionPos = transform.position;
				potionPos.y = potionPos.y + 1f;
				tempPotion_GO = Instantiate (soap_GO, potionPos, Quaternion.identity) as GameObject;
				saop.GetComponent<SoapController>().Use();//accessing the sprite controller of the key
				rb.velocity= new Vector2(transform.localScale.y,1)*throwforce;
				//	RB.AddForce(new Vector2(0,50));
			}
			
			if (anim.GetBool("WalkDownIdle")) {
				potionPos = transform.position;
				potionPos.y = potionPos.y + -1f;
				tempPotion_GO = Instantiate (soap_GO, potionPos, Quaternion.identity) as GameObject;
				rb.velocity= new Vector2(transform.localScale.y,-1)*throwforce;
				saop.GetComponent<SoapController>().Use();//accessing the sprite controller of the key

			}
			
			if (anim.GetBool("WalkRightIdle")) {
				potionPos = transform.position;
				potionPos.x = potionPos.x + 1f;
				tempPotion_GO = Instantiate (soap_GO, potionPos, Quaternion.identity) as GameObject;	
				rb.velocity= new Vector2(transform.localScale.x,1)*throwforce;
				saop.GetComponent<SoapController>().Use();//accessing the sprite controller of the key

			}
			
			if (anim.GetBool("WalkLeftIdle")) {
				potionPos = transform.position;
				potionPos.x = potionPos.x + -1f;
				tempPotion_GO = Instantiate (soap_GO, potionPos, Quaternion.identity) as GameObject;
				rb.velocity= new Vector2(transform.localScale.x,-1)*throwforce;
				saop.GetComponent<SoapController>().Use();//accessing the sprite controller of the key

			}
			//++potionCounter_Int;
			yield return new WaitForSeconds (5f);	
			//--potionCounter_Int;
			Debug.Log ("potion counter : " + potionCounter_Int);
			
			//When potion is dropped, the potion counter -1.
			
		}
		
	}
	IEnumerator FuckLife(){
		yield return new WaitForSeconds (0.2f);
		anim.SetBool ("PickUpLeft", false);
		anim.SetBool ("PickUpRight", false);
		anim.SetBool ("PickUpUp", false);
		anim.SetBool ("PickUpDown", false);
	}



	void time(){
		if (isPaused == true)
		{
			Time.timeScale = 1;
			isPaused = false;
		}
		else
		{
			Time.timeScale = 0;
			isPaused = true;
		}
	}	
}
