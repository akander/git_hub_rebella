﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]	//Adds the AudioSource component
public class sound : MonoBehaviour {

	public AudioClip playsound;	//The sound
	AudioSource audio;	//The Sound Player
	private AudioSource[] allAudioSources;



	void Start () {
		audio = GetComponent<AudioSource> (); //finds what to play
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Player")	//sound only activateable by player
		{
//			StopAllAudio ();	//clears all other sound
			audio.PlayOneShot(playsound, 1F);	//plays the sound
		}
	}
	public void Play()
	{
			//			StopAllAudio ();	//clears all other sound
			audio.PlayOneShot(playsound, 1F);	//plays the sound
	}



//	void StopAllAudio()
//	{
//		allAudioSources = FindObjectsOfType (typeof(AudioSource)) as AudioSource[];	//Finds everySound Player and saves them in the List
//		foreach(AudioSource audioS in allAudioSources)//goes through the allAudioSources list and saves each as one Sound Player
//		{
//			audioS.Stop ();	//stops everything from the "Every Sound Player"
//		}
//	}
}
