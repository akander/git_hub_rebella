﻿using UnityEngine;
using System.Collections;

public class HideController : MonoBehaviour {

	public Transform Player;
	public Sprite[] sprites;
	public bool pot;
	bool hiding;

	void OnTriggerEnter2D(Collider2D col)//sets the pos of the player back a few layers on enter
	{
		if (col.tag == "Player") 
		{
			Player.position = Player.position + new Vector3(0f, 0f, 10f);
			StartCoroutine(Hide ());
		}
	}


	void OnTriggerExit2D(Collider2D col)//On Exit of the trigger set the player pos back to original pos
	{
		Player.position = Player.position + new Vector3(0f,0f,-10f);
		StartCoroutine(Hide ());
	}

	IEnumerator Hide(){
		if(!pot){
			this.transform.parent.gameObject.GetComponent<SpriteRenderer>().sprite = sprites[1];
			yield return new WaitForSeconds(0.2f);
			this.transform.parent.gameObject.GetComponent<SpriteRenderer>().sprite = sprites[0];
		}
		else if(!hiding)
		{
			this.transform.gameObject.GetComponent<SpriteRenderer>().sprite = sprites[1];
			hiding = true;
		}
		else
		{
			this.transform.gameObject.GetComponent<SpriteRenderer>().sprite = sprites[0];
			hiding = false;
		}
	}
}
