﻿using UnityEngine;
using System.Collections;

public class BedController : MonoBehaviour {

	public Sprite[] sprite;
	public Transform Player;


	void Start()
	{
	
		transform.GetComponent<SpriteRenderer> ().sprite = sprite [0];
	
	}
	
	void OnTriggerEnter2D(Collider2D col)//sets the pos of the player back a few layers on enter
	{
		if (col.tag == "Player") 
		{
			Player.position = Player.position + new Vector3(0f, 0f, 10f);
			transform.GetComponent<SpriteRenderer> ().sprite = sprite [1];
			
		}
	}
	
	
	void OnTriggerExit2D(Collider2D col)//On Exit of the trigger set the player pos back to original pos
	{
		Player.position = Player.position + new Vector3(0f,0f,-10f);
		transform.GetComponent<SpriteRenderer> ().sprite = sprite [0];
	}

}
