﻿using UnityEngine;
using System.Collections;

public class FirePlaceController : MonoBehaviour {

	public Transform coffin_tr;

	public void Ignore() {
		Physics2D.IgnoreCollision(coffin_tr.GetComponent<Collider2D>(), this.gameObject.GetComponent<Collider2D>());
	}
}