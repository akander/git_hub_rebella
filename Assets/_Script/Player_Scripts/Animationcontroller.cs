﻿using UnityEngine;
using System.Collections;

public class Animationcontroller : MonoBehaviour
{

    public Animator anim;
	private float movex = 0f;
	private float movey = 0f;

    void Start()
    {
        anim = GetComponent<Animator>();

    }

    void FixedUpdate()
    {
		movex = Input.GetAxis ("Horizontal");
		movey = Input.GetAxis ("Vertical");
		
				if (movex > 0) {
					anim.SetBool ("WalkDownIdle", false);
					anim.SetBool ("WalkLeftIdle", false);
					anim.SetBool ("WalkUpIdle", false);
					anim.SetBool ("WalkRightIdle", true);
				}
				if (movex < 0) {
					anim.SetBool ("WalkUpIdle", false);
					anim.SetBool ("WalkDownIdle", false);
					anim.SetBool ("WalkRightIdle", false);
					anim.SetBool ("WalkLeftIdle", true);
				}
				if (movey > 0) {
					anim.SetBool ("WalkDownIdle", false);
					anim.SetBool ("WalkRightIdle", false);
					anim.SetBool ("WalkLeftIdle", false);
					anim.SetBool ("WalkUpIdle", true);
				}
				if (movey < 0) {
					anim.SetBool ("WalkUpIdle", false);
					anim.SetBool ("WalkRightIdle", false);
					anim.SetBool ("WalkLeftIdle", false);
					anim.SetBool ("WalkDownIdle", true);
				}
//
//
		if (movey > 0) {
			anim.SetBool ("WalkUp", true);
		} else {
			anim.SetBool ("WalkUp", false);
		}

		if (movey < 0) {
			anim.SetBool ("WalkDown", true);
		} else {
			anim.SetBool ("WalkDown", false);
		}

		if (movex > 0) {
			anim.SetBool ("WalkRight", true);
		} else {
			anim.SetBool ("WalkRight", false);
		}

		if (movex < 0) {
			anim.SetBool ("WalkLeft", true);
		} else {
			anim.SetBool ("WalkLeft", false);
		}
	}
	public void Weed(){
		if (anim.GetBool("WalkUpIdle"))
		{
			anim.SetBool ("PickUpUp", true);
		} else {
			anim.SetBool ("PickUpUp", false);
		}
		if (anim.GetBool("WalkRightIdle")|| anim.GetBool("WalkRight"))
		{
			anim.SetBool ("PickUpRight", true);
		} else { 
			anim.SetBool ("PickUpRight", false);
		}
		if (anim.GetBool("WalkLeftIdle"))
		{
			anim.SetBool ("PickUpLeft", true);
		} else {
			anim.SetBool ("PickUpLeft", false);
		}
		if (anim.GetBool("WalkDownIdle"))
		{
			anim.SetBool ("PickUpDown", true);
		} else {
			anim.SetBool ("PickUpDown", false);
		}
	}
	public void PickUp(){
	Weed();
	}
}


