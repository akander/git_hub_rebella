﻿using UnityEngine;
using System.Collections;

public class RailsScript : MonoBehaviour {

	public Transform player;
	public Transform prince;
	
	void Start() {
		Physics2D.IgnoreCollision(player.GetComponent<Collider2D>(), this.gameObject.GetComponent<Collider2D>());
		Physics2D.IgnoreCollision(prince.GetComponent<Collider2D>(), this.gameObject.GetComponent<Collider2D>());
	}
}
