﻿using UnityEngine;
using System.Collections;

public class Chest_Controller : MonoBehaviour {

	public Sprite[] sprite;
	public Transform Player;
	
	void OnTriggerEnter2D(Collider2D col)//sets the pos of the player back a few layers on enter
	{
		if (col.tag == "Player") 
		{
			Player.position = Player.position + new Vector3(0f, 0f, 10f);
		}
	}
	
	
	void OnTriggerExit2D(Collider2D col)//On Exit of the trigger set the player pos back to original pos
	{
		Player.position = Player.position + new Vector3(0f,0f,-5f);
	}
}
