﻿using UnityEngine;
using System.Collections;

public class FightPAniScript : MonoBehaviour {

	public Animator anim;
	
	// Update is called once per frame
	void Start () {
		
		anim = GetComponent<Animator> ();
		
	}
	// Use this for initialization
	public void FightCloud () {
		
		Debug.Log ("Fighting prince Cloud");
		anim.SetBool("Fight", true);
		
	}

}
