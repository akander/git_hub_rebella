﻿using UnityEngine;
using System.Collections;

public class PotionController : MonoBehaviour {
	public Sprite[] keys;//make the key sprite array
	public int gotten = 0;//how many keys you have
	
	
	
	// Use this for initialization
	void Start () {
		transform.GetComponent<SpriteRenderer> ().sprite = keys [gotten];//get the sprites form the array
	}
	
	public void GetPotion (){//get your key and change the sprite
		gotten++;
		if (gotten >= -1) {
			
			transform.GetComponent<SpriteRenderer> ().sprite = keys [gotten];
		}
		
	}
	
	public void UsePotion (){//use your key and change the sprite
		gotten--;
		if (gotten >= -1) {
			
			transform.GetComponent<SpriteRenderer> ().sprite = keys [gotten];
		}
		
	}
}
