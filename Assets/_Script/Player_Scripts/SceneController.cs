﻿using UnityEngine;
using System.Collections;

public class SceneController : MonoBehaviour {

    public string level = ""; 
	public Transform healthReset;
	
	void Start () {


	}

	void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Player")
        {	
			if(level == "Level 1.1")
			{
				Application.LoadLevel ("1A_Level");
//				healthReset.GetComponent<player_controller>().Startgame ();
//				int i = Random.Range(1,2) + 1;
//				if (i == 1){
//					Application.LoadLevel ("1B_Level");
//				}
//				if (i == 2){
//					Application.LoadLevel ("1A_Level");
//				}
			}
			if(level == "Level 1.2")
			{
				Application.LoadLevel ("1D_Level");
//				int i = Random.Range(1,2) + 1;
//				if (i == 1){
//					Application.LoadLevel ("1C_Level");
//				}
//				if (i == 2){
//					Application.LoadLevel ("1D_Level");
//				}
			}
			if(level == "Level Prince 1")
			{
				Application.LoadLevel ("1G_Level");
			}
			if(level == "Level 2.1")
			{
			Application.LoadLevel ("2C_Level");
				healthReset.GetComponent<player_controller>().Startgame ();
//				int i = Random.Range(1,2) + 1;
//				if (i == 1){
//					Application.LoadLevel ("2A_Level");
//				}
//				if (i == 2){
//					Application.LoadLevel ("2B_Level");
//				}
			}
			if(level == "Level 2.2")
			{
				Application.LoadLevel ("2D_Level");
//				int i = Random.Range(1,2) + 1;
//				if (i == 1){
//					Application.LoadLevel ("2C_Level");
//				}
//				if (i == 2){
//					Application.LoadLevel ("2D_Level");
//				}
			}
			if(level == "Level Prince 2")
			{
				Application.LoadLevel ("2F_Level");
//				int i = Random.Range(1,2) + 1;
//				if (i == 1){
//					Application.LoadLevel ("1E_Level");
//				}
//				if (i == 2){
//					Application.LoadLevel ("3B_Level");
//				}
			}
			if(level == "Level 3.1")
			{
				healthReset.GetComponent<player_controller>().Startgame ();
//				int i = Random.Range(1,2) + 1;
//				if (i == 1){
//					Application.LoadLevel ("1E_Level");
//				}
//				if (i == 2){
					Application.LoadLevel ("3B_Level");
//				}
			}
			if(level == "Level 3.2")
			{
//				int i = Random.Range(1,2) + 1;
//				if (i == 1){
					Application.LoadLevel ("3D_Level");
//				}
//				if (i == 2){
//					Application.LoadLevel ("3E_Level");
//				}
			}
//			if(level == "Level Prince 2")
//			{
//				Application.LoadLevel ("3G_Level");
//			}

			if(level == "Finish")
			{
				Application.LoadLevel ("WinScreen");
			}
		}
	

    }

	public void MainMenu()
	{

		Application.LoadLevel ("MainMenu");

	}

	public void LevelSelect()
	{
		healthReset.GetComponent<player_controller>().Startgame ();
		Time.timeScale = 1.0f;
		Application.LoadLevel("LevelSelect");
	}

	public void StartGame()
	{
		healthReset.GetComponent<player_controller>().Startgame ();
		Time.timeScale = 1.0f;
		int i = Random.Range(0,2);
		Application.LoadLevel ("0_Level");
	}

	public void Tutorial()
	{
		Application.LoadLevel ("Tutorial");
	}
	public void TutorialNew()
	{
		Application.LoadLevel ("0_Level");
	}

	public void Level1(){
		Application.LoadLevel ("1A_Level");
	}
	public void Level1B(){
		Application.LoadLevel ("1B_Level");
	}
	public void Level1E(){
		Application.LoadLevel ("1E_Level");
	}
	public void Level1C(){
		Application.LoadLevel ("1C_Level");
	}
	public void Level1D(){
		Application.LoadLevel ("1D_Level");
	}
	public void Level1G(){
		Application.LoadLevel ("1G_Level");
	}


	public void Level2(){
		Application.LoadLevel ("2A_Level");
	}
	public void Level2B(){
		Application.LoadLevel ("2B_Level");
	}
	public void Level2C(){
		Application.LoadLevel ("2C_Level");
	}
	public void Level2D(){
		Application.LoadLevel ("2D_Level");
	}
	public void Level2E(){
		Application.LoadLevel ("2E_Level");
	}


	public void Level3(){
		Application.LoadLevel ("3A_Level");
	}
	public void Level3B(){
		Application.LoadLevel ("3B_Level");
	}
	public void Level3C(){
		Application.LoadLevel ("3C_Level");
	}
	public void Level3D(){
		Application.LoadLevel ("3D_Level");
	}
	public void Level3E(){
		Application.LoadLevel ("3E_Level");
	}
	public void Level3G(){
		Application.LoadLevel ("3G_Level");
	}
	public void Level4(){
		Application.LoadLevel ("4_level");
	}
	public void Options(){
		Application.LoadLevel ("Options");
	}

	public void Potions(){
		Application.LoadLevel ("Rebella_Grab&Potion");
	}

//	public void Hats(){
//		Application.LoadLevel ("Hats");
//	}
//	public void Hats(){
//		Application.LoadLevel ("prince_rose");
//	}

    // Use this for initialization

	// Update is called once per frame
	void Update () {
	
	}
}
