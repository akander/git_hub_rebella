﻿using UnityEngine;
using System.Collections;

public class HealthScript : MonoBehaviour {
	public Sprite[] lives;
	public static int health = 3;



	// Use this for initialization
	void Start () {
		transform.GetComponent<SpriteRenderer> ().sprite = lives [health];
	}

	public void LoseHealth (){
		health--;
		if (health >= -1) {

			transform.GetComponent<SpriteRenderer> ().sprite = lives [health];
		}

	}

	public void FullHealth (){
		health = 3;
		transform.GetComponent<SpriteRenderer> ().sprite = lives [health];
		
	}


	// Update is called once per frame
	void Update () {
	
	}
}
