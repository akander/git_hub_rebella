﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]

public class PlayerSound : MonoBehaviour {
	
	public AudioClip step;
	public AudioClip crawl;
	private AudioSource audio;
	private float timeSinceStep = 0;
	public float freq = 0.55f;

	void Start()
	{
		audio = GetComponent<AudioSource>();
	}


	// Update is called once per frame
	void Update () {

		if ((Input.GetAxis ("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) && Time.timeSinceLevelLoad > timeSinceStep + freq)
		{
			if (gameObject.transform.position == new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, 10f))
			{
				timeSinceStep = Time.timeSinceLevelLoad;
				audio.PlayOneShot(crawl);
			}
			else
			{
				timeSinceStep = Time.timeSinceLevelLoad;
				audio.PlayOneShot (step);
			}
		}
	}
}
