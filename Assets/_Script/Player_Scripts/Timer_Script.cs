﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer_Script : MonoBehaviour {

	public Text Timer;

	// Use this for initialization
	void Start () {
		Timer.text = "Your Time: " + Time.time+ " Seconds.";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
