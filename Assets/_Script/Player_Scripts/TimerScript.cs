﻿using UnityEngine;
using System.Collections;

public class TimerScript : MonoBehaviour {

	public static int points = 100;

	public static int myTime = 0;

	// Use this for initialization
	void Start () {
		points = 0;
		myTime = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{

		StartCoroutine("countPoints");
		StartCoroutine ("countTime");




	}

	IEnumerator countPoints()
	{
		while (points > 0) {
			yield return new WaitForSeconds(1);
			points++;
		}
	}
	IEnumerator countTime()
	{
		while (myTime > 0) {
			yield return new WaitForSeconds(1);
			myTime++;
		}
	}




}
