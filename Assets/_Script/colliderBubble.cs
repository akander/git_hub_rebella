﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class colliderBubble : MonoBehaviour
{
	Transform Player;
	public float Range = 5f;
	public Transform Enemy1;

    public GameObject bubble;

	public GameObject bubble2;

	public float timer = 2.5f;
	public float timer2 = 2.5f;

	public bool dist = false; 
	public bool dist3 = false; 
	public bool collider = false;


	void Start()
    {
        bubble.SetActive(false);
		bubble2.SetActive(false);
		Player = this.transform;
    }



	void Update ()
	{
		if(dist)
		{
			if (Vector2.Distance (transform.position, Enemy1.position) <= Range)
			{	 
					bubble.SetActive (true);
					Destroy (bubble, timer);
					StartCoroutine(TurnOff());
				if(bubble2 != null){
					StartCoroutine(bubbleTwo());
				}
						
			}
		}
		if(dist3)
		{
			if (Vector3.Distance (transform.position, Enemy1.position) <= Range)
			{	 
				bubble.SetActive (true);
				Destroy (bubble, timer);
				StartCoroutine(TurnOff());
				if(bubble2 != null){
					StartCoroutine(bubbleTwo());
				}
				
			}
		}
	}
	IEnumerator bubbleTwo(){

		yield return new WaitForSeconds (timer + 0.2f);
		bubble2.SetActive (true);
		Destroy (bubble2, timer2);

	}


    void OnTriggerEnter2D(Collider2D info)
    {
       if (collider) 
		{
        	bubble.SetActive(true);
        	Destroy(bubble, timer);
			StartCoroutine(TurnOff());
			if(bubble2 != null){
				StartCoroutine(bubbleTwo());

			}
    
		}
	}


	IEnumerator TurnOff(){

		yield return new WaitForSeconds (timer + timer2 + 2f);
		this.gameObject.SetActive(false);

	}




}